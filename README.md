Austin Bodzas - Dotfiles
===

Distro: [Arch Linux](https://www.archlinux.org)

Window Manager: [i3-gaps](https://github.com/Airblader/i3) or on AUR "i3-gaps-git"

Some Dependencies:
---
[neovim](https://github.com/neovim/neovim)

[rofi](https://github.com/neovim/neovim)

[urxvt](https://wiki.archlinux.org/index.php/rxvt-unicode)

[gohufont](font.gohu.org) or on aur "gohufont"

[compton](https://wiki.archlinux.org/index.php/Compton)

[font awesome](https://fortawesome.github.io/Font-Awesome/) or on aur "ttf-font-awesome"

