call plug#begin()

Plug 'scrooloose/nerdtree'
Plug 'Shougo/deoplete.nvim'
Plug 'jiangmiao/auto-pairs'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set nohlsearch
set ruler
set showcmd
set showmode

filetype plugin indent on
syntax on
set number


" deoplete enable
let g:deoplete#enable_at_startup = 1

"let g:airline_powerline_fonts = 1
colorscheme sourcerer

